package main.java;

public interface IContactDao {

    void add(String name);

    boolean isContactExist(String name);
}

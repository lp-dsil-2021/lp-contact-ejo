package main.java;

import java.util.ArrayList;
import java.util.List;

public class ContactDao implements IContactDao{

    private List<Contact> contacts = new ArrayList<>();

    @Override
    public void add(String name) {
        contacts.add(new Contact(name));
    }

    @Override
    public boolean isContactExist(String name){
        return contacts.stream().anyMatch(x -> name.equalsIgnoreCase(x.getName()));
    }

}
